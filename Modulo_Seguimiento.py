from scipy.spatial import distance as dist
from collections import OrderedDict
import numpy as np

from Filtro_Kalman import Kalman



class Elemento(object):  # clase de cada objeto que va a ser trackeado

    def __init__(self, centroide_actual, ID):
        self.myID = ID
        self.KF = Kalman()#centroide_actual)

        self.centroide = centroide_actual
        self.prediccion = centroide_actual  # centroide predicho (x,y), en un inicio es el mismo
        self.desapariciones=0  # numero de veces que lleva el elemento trackeado sin volver a a aparecer en otro frame
        self.traza = []  # recorrido


class myTracker():
    def __init__(self, maximaDesaparicion, maximaDistancia, maximaTraza, predictor, tam):

        # id que se va a signando a cada elemento
        self.siguienteID = 0
        # diccionario que almacena los elementos trackeables
        self.elementos = OrderedDict()

        self.maximaDistancia = maximaDistancia #distancia maxima de un centroide actual al anterior para deducir la asignacion de ID
        self.maximaDesaparicion = maximaDesaparicion  # numero maximo de frames que un elemento lleva sin aparecer para desregistrarlo o no
        self.maximaTraza = maximaTraza  #distancia maxima de la traza de movimiento del objeto

        self.predictor = predictor
        self.TAM = tam

    def nuevoElemento(self, frame, centroideElemento, w, h):
        # guardo el centroide del nuevo elemento a trackear
        self.elementos[self.siguienteID] = Elemento(centroideElemento,self.siguienteID)

        self.predictor.nuevoHumano(self.siguienteID, frame, centroideElemento, w, h)

        self.siguienteID += 1

    def quitarElemento(self, ID):

        del self.elementos[ID]
        self.predictor.quitarHumano(ID)

    def tracker(self, cajas, frame):

        # en caso de que no existan detecciones
        if len(cajas) == 0:
            # marcamos cada uno de los elementos trackeables almacenados como desaparecidos en su contador de desapariciones
            for ID in list(self.elementos.keys()):
                self.marcarDesaparicion(ID)
            # devolvemos los objetos que teniamos ya
            return self.elementos


        # si si que existen detecciones
        centroides = self.cajas_centroides(cajas)
        # Si aun no teniamos elementos trackeables añadimos los nuevos
        if len(self.elementos) == 0:
            for i in range(0, len(centroides)):
                self.nuevoElemento(frame, centroides[i], self.TAM, self.TAM)
        else:
            # si ya tenemos elementos trackeables tengo que comparar sus centroides para encontrar las correlaciones

            # obtengo las ID de mis elementos trackeables
            elementosID = list(self.elementos.keys())

            # obtengo los centroides asignados a esas IDs

            elementos = self.elementos.values()
            elementosCentroides=[]
            for elemento in elementos:
                if elemento.desapariciones> 0:
                    elementosCentroides.append(elemento.prediccion)
                else:
                    elementosCentroides.append(elemento.centroide)

            # calculo de las distancias euclideas entre los diferentes centroides
            # quedando una matriz de la forma de la forma filas->elementos ya almacenados / columnas->elementos recien detectados

            D = dist.cdist(np.array(elementosCentroides), centroides)

            # de cada fila obtenemos la columna con menor distancia y cogemos los indices ordenados de dicha lista
            fils = D.min(axis=1).argsort()

            # obtengo los indices de columna para cada una de las distancias ordenadas de menor a mayor
            cols = D.argmin(axis=1)[fils]

            # para saber si necesitamos crear, actualizar o eliminar un elemento
            # tengo que saber que filas y columnas he examinado ya
            examFils = set()
            examCols = set()

            asignaciones = ([-1] * len(elementosID))

            # bucle sobre las parejas fila-columna
            for (fil, col) in zip(fils, cols):
                # si hemos examinado ya esa columna o esa fila seguimos adelante,
                # y si esa distancia es mayor que la permitida, el elemento detectado no debe corresponder a ese mismo elemento almacenado
                if fil in examFils or col in examCols or D[fil][col] > self.maximaDistancia:
                    continue

                objectID = elementosID[fil]

                preAux = self.elementos[objectID].KF.predecir(self.elementos[objectID].centroide)

                self.elementos[objectID].centroide = centroides[col]
                self.elementos[objectID].desapariciones=0
                preAux = self.elementos[objectID].KF.predecir(centroides[col])

                self.elementos[objectID].prediccion[0] = preAux[0]
                self.elementos[objectID].prediccion[1] = preAux[1]
				
				#actualizacion del humano en el modulo de prediccion
                self.predictor.actualizarHumano(objectID, frame, centroides[col], self.TAM, self.TAM)

                if len(self.elementos[objectID].traza) > self.maximaTraza:
                    self.elementos[objectID].traza.pop(0)  # elimino el primer frame que tenia almacenado , asi puedo meter uno mas

                self.elementos[objectID].traza.append(self.elementos[objectID].centroide)

                # marco fila y columna como examinadas
                examFils.add(fil)
                examCols.add(col)

            # Ahora nos centramos en las parejas fila-columna no examinadas

            no_examFils = set(range(0, D.shape[0])).difference(examFils) #mis elementos no asignados
            no_examCols = set(range(0, D.shape[1])).difference(examCols) #nuevas detecciones no asignadas

            # comprobamos cuales han desaparecido para marcarlos como tal
            for fil in no_examFils:
                # obtiene el id del elemento almacenado correspondiente a esa fila y aumenta sus desapariciones
                objectID = elementosID[fil]
                eliminado=self.marcarDesaparicion(objectID)
                if eliminado:
                    del asignaciones[fil]
            #comprobamos los nuevas detecciones sin asignar para marcarlas como nuevos elementos
            for col in no_examCols:
                 self.nuevoElemento(frame, centroides[col], self.TAM, self.TAM)

        return self.elementos

		
    def predecirAccion(self, ID_list):
        labels = OrderedDict()

        for id, c in ID_list.items():
            labels[id] = self.predictor.predecirAccion(self.predictor.personas_frames[id])

        return labels


    def marcarDesaparicion(self, ID):
        # aumentamos el contador de desapariciones del elemento
        ## seguimos prediciendo posiciones para este elemento y asignandoselas como centroides, de manera que si el elemento desaparece pero sigue moviendose podra ser captado de nuevo
        eliminado= False
        self.elementos[ID].desapariciones += 1

        preAux=self.elementos[ID].KF.soloPredecir()
        self.elementos[ID].prediccion[0] = preAux[0]
        self.elementos[ID].prediccion[1] = preAux[1]

        #self.elementos[ID].centroide = self.elementos[ID].prediccion

        # en caso de sobrepasar el maximo de frames desaparecido procedo a eliminar el elemento de las listas
        if self.elementos[ID].desapariciones > self.maximaDesaparicion:
            eliminado=True
            self.quitarElemento(ID)

        return eliminado

    def cajas_centroides(self, cajas):

        # inicializo un array de centroides a 0 , tantos centroides como cajas, tamaño numCajas x 2
        # 2 porque cada centroide se define como una coordenada(x,y)
        centroides = np.zeros((len(cajas), 2), dtype="int")

        # recorro cada caja detectada
        for (i, (X1, Y1, X2, Y2)) in enumerate(cajas):
            centroides[i] = self.caja_centroide(X1, X2, Y1, Y2)

        return centroides

    def caja_centroide(self, X1, X2, Y1, Y2):
        # el centroide se obtiene de las propias dimensiones de las cajas
        X = int((X1 + X2) / 2.0)
        Y = int((Y1 + Y2) / 2.0)

        return (X, Y)