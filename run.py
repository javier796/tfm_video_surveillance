
import argparse
from Modulo_Deteccion import modulo_Deteccion

#argument parser

ap = argparse.ArgumentParser()
ap.add_argument("-c", "--confianza", type=float, default=0.6, help="Umbral para la deteccion de objetos")
ap.add_argument("-d", "--desaparicion", type=int, default=20, help="Numero maximo permitido de desapariciones")
ap.add_argument("-dis", "--distancia", type=int, default=50, help="Distancia maxima para tolerar correspondencias")
ap.add_argument("-t", "--traza", type=int, default=100, help="Longitud maxima de la trayectoria dibujada")
ap.add_argument("-da", "--duracionAccion", type=int, default=16, help="Numero de frames necesarios para calcular la accion")
ap.add_argument("-tv", "--tamanoVentana", type=int, default=150, help="tamaño TAMxTAM de la ventana de extraccion")
ap.add_argument("-o", "--output",  type=str, default="salida_prueba.avi", help="Path de salida del video procesado")
ap.add_argument("-i", "--input",  type=str, default="video_prueba.mp4", help="Path del video de origen")
args = vars(ap.parse_args())

# lee los contenidos de los argumentos y los pasa a la clase de deteccion para su inicializacion

modulo = modulo_Deteccion(confianza=args["confianza"],
                          desaparicion=args["desaparicion"],
                          distancia=args["distancia"],
                          traza=args["traza"],
                          tam_ventana=args["tamanoVentana"],
                          duracion_accion= args["duracionAccion"]
                          )
# ejecuta el bucle de ejecucion del sistema						  
modulo.procesarVideo(path=args["input"], destino_path=args["output"])
