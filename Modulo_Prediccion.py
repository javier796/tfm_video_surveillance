import numpy as np
import imutils
import cv2
from collections import OrderedDict

CLASSES = open("Modelo_Resnet3D/action_recognition_kinetics.txt").read().strip().split("\n")

class myActionPredictor():
    def __init__(self, num_frames, size_frames):

        #diccionario que almacena las personas
        self.personas_frames = OrderedDict()
        self.accion_personas= OrderedDict()

        #numero de frames a alcanzar antes de poder detectar la actividad realizada por el humano
        self.maximosFrames = num_frames
        self.size_frames = size_frames
		
        #///////////////carga del modelo resnet-34 3D///////////////////////////
        print("Cargando modelo resnet34 preentrenado en kinetics400...")
        self.actionNet = cv2.dnn.readNet("Modelo_Resnet3D/resnet-34_kinetics.onnx")


    def nuevoHumano(self,ID, frame,centroide,w,h):

        #inicializo la lista de esa persona
        self.personas_frames[ID]= []
        #añado el primer frame recortado
        self.personas_frames[ID].append(self.recortarSeccion(frame,centroide,w,h))
        #inicializo a 0 el contador de frames
        self.accion_personas[ID] = "Por determinar"

    def actualizarHumano(self,ID,frame,centroide,w,h):
        #si en efecto intentamos actualizar un ID existente
        if ID in self.personas_frames:

            if len(self.personas_frames[ID]) == self.maximosFrames:
                self.personas_frames[ID].pop(0) #elimino el primer frame que tenia almacenado , asi puedo meter uno mas

            self.personas_frames[ID].append(self.recortarSeccion(frame,centroide,w,h))

    def quitarHumano(self, ID):

        del self.personas_frames[ID]
        del self.accion_personas[ID]

    def recortarSeccion(self, frame,centroide,w,h):
        H,W,c=frame.shape
        print(W,H)

        cortesX=[int(centroide[0]-w/2),int(centroide[0]+w/2)]
        cortesY=[int(centroide[1]-h/2),int(centroide[1]+h/2)]



        #limites corte para el eje X-------------------------------------------------------------
        if (centroide[0]-w/2) <0: #si estamos demasiado pegados al borde izquierdo
            cortesX[0]=0 #cortamos desde el borde izquierdo
            cortesX[1]=w #hasta el ancho del corte pedido
        else:
            if (centroide[0] + w / 2 ) >= W: #si estamos demasiado cerca del borde derecho
                cortesX[0]= W - w #cortamos desde el borde derecho
                cortesX[1] = W  # hasta el largo pedido

        # limites corte para el eje Y------------------------------------------------------------
        if (centroide[1] - h / 2) < 0:  # si estamos demasiado pegados al borde superior
            cortesY[0] = 0  # cortamos desde el borde superior
            cortesY[1] = h  # hasta el largo del corte pedido
        else:
            if (centroide[1] + h / 2) >= H:  # si estamos demasiado cerca del borde de abajo
                cortesY[0] = H - h
                cortesY[1] = H
        print(centroide[0], centroide[1])

        salida=frame[cortesY[0]:cortesY[1], cortesX[0]:cortesX[1]]

        return   imutils.resize(salida, width=400) #devuelvo el frame recortado por los puntos requeridos


    def predecirAccion(self,frames):

        if len(frames)< self.maximosFrames :
            return "Por determinar"
        else:

            # construimos blob de conjunto de imagenes redimensionado para usar en la red
            blob = cv2.dnn.blobFromImages(frames, 1.0, (self.size_frames, self.size_frames), (114.7748, 107.7354, 99.4750), swapRB=True, crop=True)
            blob = np.transpose(blob, (1, 0, 2, 3))
            blob = np.expand_dims(blob, axis=0)

            # metemos el blob en la red para obtener la prediccion
            self.actionNet.setInput(blob)
            outputs = self.actionNet.forward()
            label = CLASSES[np.argmax(outputs)]
            return label