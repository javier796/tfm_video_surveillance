import numpy as np
import time
import cv2
from Modulo_Seguimiento import myTracker
from Modulo_Prediccion import myActionPredictor
import keyboard

class modulo_Deteccion():
    def __init__(self, desaparicion=20, distancia=50, traza=100, tam_ventana=150, confianza=0.6, duracion_accion=16):
        self.LABELS_OBJECTS = self.cargar_etiquetas()
        self.COLORS = self.cargar_colores()
        self.net = self.cargar_modelo_deteccion()
        self.ln = self.leer_capas()

        self.CONFIANZA = confianza
        self.LIMITE = 0.3

        # /////////////////////////////// Predictor de acciones ////////////////////////////////////////////
        self.DURATION = duracion_accion
        self.SAMPLE_SIZE = 114
        predictor = self.inicializar_modulo_prediccion()

        # ///////////////////////////////////// Tracker //////////////////////////////////
        self.TAM = tam_ventana
        self.MAX_DESAPARICION = desaparicion
        self.MAX_DISTANCIA = distancia
        self.MAX_TRAZA = traza
        self.tracker = self.inicializar_modulo_seguimiento(predictor)

    def cargar_etiquetas(self):
        return open("Modelo_Yolov3/coco.names").read().strip().split("\n")

    def cargar_colores(self):
        np.random.seed(42)
        return np.random.randint(0, 255, size=(len(self.LABELS_OBJECTS), 3), dtype="uint8")

    def cargar_modelo_deteccion(self):
        weightsPath = "Modelo_Yolov3/yolov3.weights"
        configPath = "Modelo_Yolov3/yolov3.cfg"

        # carga del modelo preentrenado
        print("Cargando el modelo de deteccion de objetos YOLO..")
        return cv2.dnn.readNetFromDarknet(configPath, weightsPath)

    def leer_capas(self):
        ln_aux = self.net.getLayerNames()
        return [ln_aux[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]

    def inicializar_modulo_seguimiento(self, predictor):
        return myTracker(self.MAX_DESAPARICION, self.MAX_DISTANCIA, self.MAX_TRAZA, predictor, self.TAM)

    def inicializar_modulo_prediccion(self):
        return myActionPredictor(self.DURATION, self.SAMPLE_SIZE)

    def procesarVideo(self, path, destino_path):
        print("Accediendo al video...")
        vs = cv2.VideoCapture(path)

        writer = None
        (W, H) = (None, None)

        # Recorremos el video
        frames = []
        ix = 0
        # tracker.

        track_colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0),
                        (0, 255, 255), (255, 0, 255), (255, 127, 255),
                        (127, 0, 255), (127, 0, 127)]

        while True:
            # leo el siguiente frame
            ix = ix + 1
            print(ix)
            # if ix>1000: break

            (grabbed, frame) = vs.read()
            if grabbed:

                if W is None or H is None:
                    (H, W) = frame.shape[:2]

                # construye la imagen como blob para usarla de input en la red
                blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416), swapRB=True, crop=False)

                # realizacion de la prediccion de detecciones con el modelo
                self.net.setInput(blob)
                start = time.time()
                layerOutputs = self.net.forward(self.ln)
                end = time.time()

                # iniciamos los contenedores de las detecciones
                boxes = []
                confidences = []
                classIDs = []

                # recorremos layer outputs
                for output in layerOutputs:
                    # loop over each of the detections
                    for detection in output:
                        # extrae la clase de la prediccion y su probabilidad
                        scores = detection[5:]
                        classID = np.argmax(scores)
                        confidence = scores[classID]

                        # Al asegurarnos que las detecciones sobrepasan a probabilidad minima nos libramos de probabilidades debiles
                        if confidence > self.CONFIANZA:
                            # YOLO devuelve las coordenadas centrales unicamente junto con la altura y anchura de la caja

                            box = detection[0:4] * np.array([W, H, W, H])
                            (centerX, centerY, width, height) = box.astype("int")

                            # derivamos las coordenadas de la esquina superior izquierda a partir del centro y sus tamaños
                            x = int(centerX - (width / 2))
                            y = int(centerY - (height / 2))

                            # añadimos a nuestra lista de cajas las coordenadas
                            # confidences, and class IDs
                            boxes.append([x, y, int(width), int(height)])
                            confidences.append(float(confidence))
                            classIDs.append(classID)

                # Al aplicar supresion podemos deshacernos de las cajas con una confianza de deteccion baja y que se superpongan
                idxs = cv2.dnn.NMSBoxes(boxes, confidences, self.CONFIANZA, self.LIMITE)

                personas = 0
                cajas = []

                # si al menos se ha detectado algo
                if len(idxs) > 0:

                    # bucle sobre los  objetos detectados
                    for i in idxs.flatten():
                        if self.LABELS_OBJECTS[classIDs[i]] == "person":
                            personas = personas + 1

                            # extraigo las coordenadas asignadas a la deteccion y las apilo en una lista
                            (x, y) = (boxes[i][0], boxes[i][1])
                            (w, h) = (boxes[i][2], boxes[i][3])
                            cajas.append([x, y, x + w, y + h])

                # ejecucion de un paso de seguimiento sobre las ultimas detecciones
                detecciones = self.tracker.tracker(cajas, frame)
                # ejecucion de un paso de prediccion sobre los elementos bajo seguimiento devueltos previamente
                acciones = self.tracker.predecirAccion(detecciones)

                for (ID, track) in detecciones.items():
                    coordenada = track.centroide
                    coordenada2 = track.prediccion

                    accion = acciones[ID]
                    # formateo texto sobre cada persona
                    text = "Persona {}-{}".format(ID, accion)

                    #para ir alternando colores
                    clr = ID % 9

                    cv2.putText(frame, text, (coordenada[0] - 10, coordenada[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                                track_colors[clr], 2)
                    cv2.circle(frame, (coordenada[0], coordenada[1]), 4, track_colors[clr], -1)

                    #pintado de la coordenada de prediccion
                    cv2.putText(frame, "Predict {}".format(ID), (coordenada2[0] - 10, coordenada2[1] - 10),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.4, (30, 255, 255), 2)
                    cv2.circle(frame, (coordenada2[0], coordenada2[1]), 2, (30, 255, 255), -1)

                    if (len(track.traza) > 1):
                        for j in range(len(track.traza) - 1):
                            # Dibujar traza de movimiento
                            x1 = track.traza[j][0]  # [0]
                            y1 = track.traza[j][1]  # [0]
                            x2 = track.traza[j + 1][0]  # [0]
                            y2 = track.traza[j + 1][1]  # [0]
                            cv2.line(frame, (int(x1), int(y1)), (int(x2), int(y2)), track_colors[clr], 2)

                texto = str(personas) + " personas detectadas"
                cv2.putText(frame, texto, (10, 200 - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)

                # si no tiene un escritor asignado aun
                if writer is None:
                    # inicializa
                    fourcc = cv2.VideoWriter_fourcc(*"MJPG")
                    writer = cv2.VideoWriter(destino_path, fourcc, 30, (frame.shape[1], frame.shape[0]), True)

                writer.write(frame)

                # si pulso q para el bucle
                if keyboard.is_pressed('p'):
                    print("La ejecucion ha finalizado")
                    break

