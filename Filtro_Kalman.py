import cv2
import numpy as np


#implementacion del filtro de kalman utilizando la libreria opencv
class Kalman():

    def __init__(self):
        self.kf = cv2.KalmanFilter(4, 2)
        self.kf.measurementMatrix = np.array([[1, 0, 0, 0], [0, 1, 0, 0]], np.float32)
        self.kf.transitionMatrix = np.array([[1, 0, 1, 0], [0, 1, 0, 1], [0, 0, 1, 0], [0, 0, 0, 1]], np.float32)

    def predecir(self,coords):
        pos_X= coords[0]
        pos_Y=coords[1]

        measured = np.array([[np.float32(pos_X)], [np.float32(pos_Y)]])
        #corrige en funcion de la medida actual
        self.kf.correct(measured)
        #realiza una prediccion teniendo en cuenta las anteriores y la actual
        prediccion = self.kf.predict()

        return prediccion

    def soloPredecir(self):
        return self.kf.predict()